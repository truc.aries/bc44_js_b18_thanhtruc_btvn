var numArr = [];
document.getElementById("btnThemSo").onclick = function () {
  var numberEl = document.querySelector("#nhapSo");
  var number = numberEl.value * 1;

  //   Thêm số vào mảng:
  numArr.push(number);
  numberEl.value = "";
  document.getElementById("numArr").innerHTML = numArr;
};

//   1/ Tính tổng số dương trong mảng
document.getElementById("btnBai1").onclick = function () {
  var tongSoDuong = 0;
  for (var index = 0; index < numArr.length; index++) {
    var currentNumber = numArr[index];

    if (currentNumber > 0) {
      tongSoDuong += currentNumber;
    }
  }
  document.getElementById("ketQuaBai1").innerHTML = tongSoDuong;
};

//   2/ Đếm số dương trong mảng
document.getElementById("btnBai2").onclick = function () {
  var soLuongSoDuong = 0;
  for (var index = 0; index < numArr.length; index++) {
    var currentNumber = numArr[index];

    if (currentNumber > 0) {
      soLuongSoDuong++;
    }
  }
  document.getElementById("ketQuaBai2").innerHTML = soLuongSoDuong;
};

//   3/ Tìm số nhỏ nhất trong mảng
document.getElementById("btnBai3").onclick = function () {
  var soNhoNhat = Math.min(...numArr);
  document.getElementById("ketQuaBai3").innerHTML = soNhoNhat;
};

//   4/ Tìm số dương nhỏ nhất trong mảng
document.getElementById("btnBai4").onclick = function () {
  // Tạo array mới lưu các số dương
  var arraySoDuong = [];
  for (var i = 0; i < numArr.length; i++) {
    if (numArr[i] > 0) {
      arraySoDuong.push(numArr[i]);
    }
  }
  // Kiểm tra mảng mới có phần tử hay không
  if (arraySoDuong.length > 0) {
    var soDuongNhoNhat = arraySoDuong[0];
    // Tìm giá trị dương nhỏ nhất trong mảng mới
    for (var i = 1; i < arraySoDuong.length; i++) {
      if (arraySoDuong[i] < soDuongNhoNhat) {
        soDuongNhoNhat = arraySoDuong[i];
      }
    }
    document.getElementById("ketQuaBai4").innerHTML = soDuongNhoNhat;
  } else {
    document.getElementById("ketQuaBai4").innerHTML =
      "Không có số dương trong mảng";
  }
};

//   5/ Tìm số chẵn cuối cùng trong mảng. Nếu không có số chẵn trả về -1
document.getElementById("btnBai5").onclick = function () {
  var ketQuaBai5 = document.getElementById("ketQuaBai5");
  var soChanCuoi = numArr.filter((number) => number % 2 === 0).pop();
  ketQuaBai5.innerHTML = soChanCuoi !== undefined ? soChanCuoi : "-1";
};

//   6/ Đổi chỗ 2 giá trị trong mảng
document.getElementById("btnBai6").onclick = function () {
  var viTri1 = document.getElementById("viTri1").value * 1;
  var viTri2 = document.getElementById("viTri2").value * 1;
  var x = numArr[viTri1];
  numArr[viTri1] = numArr[viTri2];
  numArr[viTri2] = x;
  document.getElementById("ketQuaBai6").innerHTML = `
Vị trí sau khi đổi: ${numArr}
`;
};

//  7/ Sắp xếp theo thứ tự tăng dần
document.getElementById("btnBai7").onclick = function () {
  numArr.sort();
  document.getElementById("ketQuaBai7").innerHTML = numArr;
};

//  8/ Tìm số nguyên tố đầu tiên trong mảng. Nếu không có trả về -1
document.getElementById("btnBai8").onclick = function () {
  for (var i = 0; i < numArr.length; i++) {
    if (soNguyenTo(numArr[i])) {
      document.getElementById("ketQuaBai8").innerHTML = numArr[i];
      break;
    }
    if (i === numArr.length - 1) {
      document.getElementById("ketQuaBai8").innerHTML = "-1";
    }
  }
};
function soNguyenTo(n) {
  if (n <= 1) {
    return false;
  }
  for (var i = 2; i <= Math.sqrt(n); i++) {
    if (n % i === 0) {
      return false;
    }
  }
  return true;
}

//  9/ Nhập thêm 1 mảng số thực. Trong mảng có bao nhiêu số nguyên
var newnumArr = [];
document.getElementById("btnThemSoBai9").onclick = function () {
  var numberEl9 = document.querySelector("#nhapThemSo");
  var number9 = numberEl9.value * 1;

  //   Thêm mảng mới vào mảng cũ:
  newnumArr.push(number9);
  numberEl9.value = "";
  var combineArray = numArr.concat(newnumArr);
  document.getElementById("ketQuaThemSoBai9").innerHTML = `
  Mảng mới: ${combineArray}
  `;
};

// Đếm số nguyên trong mảng mới
document.getElementById("btnBai9").onclick = function () {
  var soLuongSoNguyen = 0;
  for (var i = 0; i < combineArray.length; i++) {
    var currentNumber9 = combineArray[i];

    if (combineArray.filter((num) => Number.isInteger(num)).length === true) {
      soLuongSoNguyen++;
    }
  }
  document.getElementById("ketQuaBai9").innerHTML = soLuongSoNguyen;
};

// 10/ So sánh số lượng số âm và số lượng số dương
document.getElementById("btnBai10").onclick = function () {
  var soLuongSoDuong = 0;
  var soLuongSoAm = 0;

  numArr.forEach((number) => {
    if (number > 0) {
      soLuongSoDuong++;
    } else if (number < 0) {
      soLuongSoAm++;
    }
  });

  if (soLuongSoDuong > soLuongSoAm) {
    document.getElementById("ketQuaBai10").innerHTML = `Số dương > Số âm`;
  } else if (soLuongSoAm > soLuongSoDuong) {
    document.getElementById("ketQuaBai10").innerHTML = `Số dương < Số âm`;
  } else {
    document.getElementById("ketQuaBai10").innerHTML = `Số dương = Số âm`;
  }
};
